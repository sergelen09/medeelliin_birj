import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mbirj/homePage.dart';
import 'controller/http.dart';
import 'globals.dart' as globals;
import 'package:http/http.dart' as http;

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mbirj',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => new MyHomePage(title: 'Нэвтрэх'),
        '/category': (context) => CategoryPage(), 
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}
//App нээгдэх үед nonce-г хэрэглэгчрүү буцаана.
class _MyHomePageState extends State<MyHomePage> {

  Future getUsers() async {
    http.Response responses = await http.get("http://zar.mbirj.mn/rest/type.php?action=select&username=" +globals.name.text +"&password=" +globals.pass.text);
    globals.xml = json.decode(responses.body);
    globals.list = globals.xml.values.toList();

      String md = globals.name.text+"|"+globals.list[2]+"|"+ globals.pass.text;
    //Нэвтрэх button дарагдах үед hash-г үүсгээд category-г дуудна.   Future getResponse() async{}
    http.Response response = await http.get("http://zar.mbirj.mn/rest/type.php?action=select&username=pos1&password=pos&nonce="+globals.list[2]+"&hash="+ generateMd5(md));
    globals.data = json.decode(response.body); //JSON байдлаар ирсэн өгөгдөл
    globals.categoryList = globals.data['data'];
    globals.list = globals.data.values.toList(); //Хүсэлтээр ирсэн өгөгдлүүдийг list-нд хадгалах
    
    print(globals.list[2]);
    print(globals.list.toString());

    print(globals.list.toString());
    
    if(globals.list[0].toString() == "200") {
      setState(() {
        _isLoading = false;
        Navigator.pushNamed(context, '/category');
      });
    }
    else{
      _showDialog();
      setState(() {
        _isLoading = false;
      });
    }
  }
  

  bool isChecked = false; // намайг сана(checkbox)
  bool _isLoading = false;
  
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        title: new Text("Мэдээллийн бирж"),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: _isLoading ? Center(child: CircularProgressIndicator()) : Container(
          color: Colors.white,
          child: Padding( 
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Нэвтрэх",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 45, color: Colors.blue),
                ),
                SizedBox(height: 50),
                TextField(
                  controller: globals.name,
                  obscureText: false,
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Хэрэглэгчийн нэр",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0))),
                ),
                SizedBox(height: 20),
                TextField(
                  controller: globals.pass,
                  obscureText: true,
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Нууц үг",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0))),
                ),
                SizedBox(height: 5),
                CheckboxListTile(
                  title: Text("Намайг сана"),
                  activeColor: Colors.white,
                  checkColor: Colors.blue,
                  value: isChecked,
                  controlAffinity: ListTileControlAffinity.leading,
                  onChanged: (value) {
                   setState(() {
                      isChecked = value;
                    });
                  },
                ),
                RaisedButton(
                  elevation: 5.0,
                  color: Colors.blue,
                  padding: EdgeInsets.fromLTRB(60.0, 15.0, 60.0, 15.0),
                  onPressed: (){
                    setState(() {
                      _isLoading = true;
                    });
                    getUsers();
                  },
                  child: Text(
                    "Нэвтрэх",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white),
                  ),  
                ),
                SizedBox(height: 20),
                RaisedButton(
                  elevation: 5.0,
                  color: Colors.blue,
                  padding: EdgeInsets.fromLTRB(50.0, 15.0, 50.0, 15.0),
                  onPressed: () {},
                  child: Text(
                    "Бүртгүүлэх",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  
  void _showDialog(){
    showDialog(
      context: context,
      builder: (BuildContext context){
                  return AlertDialog(
                        title: new Text( "Алдаа"),
                        content: new Text("Хэрэглэгчийн нэр эсвэл нууц үг буруу байна"),
                        actions: <Widget>[
                          new FlatButton(onPressed: (){
                            Navigator.of(context).pop();
                          }, child: new Text("Буцах"))
                        ],

                      );
      }
    );
  }
}
