import 'package:flutter/material.dart';
import 'globals.dart' as globals;

class CategoryPage extends StatefulWidget {
@override
CategoryPageState createState() => CategoryPageState();
}

class CategoryPageState extends State<CategoryPage> {

  @override
  void initState(){
    super.initState(); 
  }
  
  Widget build(BuildContext context) {
        
        return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: new AppBar(
        centerTitle: true,
        title: new Text("Ангилал"),
        backgroundColor: Colors.blue,
          ),
          body: 
            new ListView.builder(
              //scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: globals.categoryList == null ? 0 : globals.categoryList.length,
              itemBuilder: (BuildContext context, int index){
                return GestureDetector(
                  onTap: () {Scaffold.of(context).showSnackBar(SnackBar(content: Text(globals.categoryList[index]['TypeID'].toString()),));
                  },
                  child: Container(
                    child: new Row(
                      children: <Widget>[
                      new Card(
                        child: new Container(
                          height: 60,
                          width: 380,
                          child: new Text(globals.categoryList[index]['TypeName']),
                          padding: const EdgeInsets.all(20.0),
                       ),
                      )
                    ],
                  )
                )
              );
            },
          )
    );
  }
            
}
